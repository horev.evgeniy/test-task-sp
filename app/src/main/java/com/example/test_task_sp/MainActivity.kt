package com.example.test_task_sp

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.test_task_sp.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {


    private val binding: ActivityMainBinding by viewBinding()

    lateinit var app: App

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        app = application as App
        var counter = app.getAppCounter()
        binding.tvCount.text = counter.toString()
        if (counter == 3) {
            val toast = Toast.makeText(
                this,
                "это 3ий холодный запуск приложения",
                Toast.LENGTH_LONG
            )
            toast.show()

        } else {
            counter += 1
        }

        binding.btnRefresh.setOnClickListener {
            app.refreshCounter()
            binding.tvCount.text = ""
        }

    }

}