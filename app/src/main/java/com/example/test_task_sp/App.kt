package com.example.test_task_sp

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

const val CONFIG = "config"
const val COUNTER = "counter"


class App() : Application() {


    lateinit var settings: SharedPreferences
    var counter = 0


    override fun onCreate() {
        super.onCreate()
        settings = getSharedPreferences(CONFIG, Context.MODE_PRIVATE)
        counter = getAppCounter()
        counter += 1
        encreaseCounter(counter)
    }

    fun getAppCounter(): Int {
        return settings.getInt(COUNTER, 0)
    }

    fun encreaseCounter(value: Int) {
        val editor = settings.edit()
        editor.putInt(COUNTER, value)
        editor.apply()
    }

    fun refreshCounter() {
        val editor = settings.edit()
        editor.remove(COUNTER)
        editor.apply()
    }


}